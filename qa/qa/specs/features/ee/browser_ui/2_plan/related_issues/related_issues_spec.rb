# frozen_string_literal: true

module QA
  context 'Plan' do
    describe 'Related issues' do
      let(:project) do
        Resource::Project.fabricate_via_api! do |resource|
          resource.name = 'project-to-test-related-issues'
        end
      end

      before do
        Runtime::Browser.visit(:gitlab, Page::Main::Login)
        Page::Main::Login.perform(&:sign_in_using_credentials)

        @issue_1 = Resource::Issue.fabricate_via_api! do |issue|
          issue.project = project
          issue.title = 'Issue 1'
        end

        @issue_2 = Resource::Issue.fabricate_via_api! do |issue|
          issue.project = project
          issue.title = 'Issue 2'
        end
      end

      it 'relates and unrelates one issue to/from another' do
        @issue_1.visit!

        Page::Project::Issue::Show.perform do |show|
          show.relate_issue(@issue_2)

          expect(show).to have_content("marked this issue as related to ##{@issue_2.iid}")
          expect(show.related_issuable_item).to have_content(@issue_2.title)

          show.click_remove_issue_button

          expect(show).to have_content("removed the relation with ##{@issue_2.iid}")
          expect(show).not_to have_content(@issue_2.title)
        end
      end
    end
  end
end
